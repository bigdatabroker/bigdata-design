\subsubsection[AccessToken]{class \code{ AccessToken } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents an access token that is used to authenticate users. An \code{AccessToken} is associated with exactly one \code{User}, but a user can have more than one \code{AccessToken}.

\item [Attributes] \

\classattribute{user}{User}{Owner of the token.}
\classattribute{token}{String}{Contains an unique random string.}
\classattribute{created\_on}{Datetime}{Date and time when the token was generated.}
\classattribute{expires\_on}{Datetime}{Date and time when the token will not be valid anymore. Is \NULL if the token will never expire.}
\classattribute{data}{String}{Contains authentication data such as username, passwords or certificates.}


\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}





\subsubsection[Event]{class \code{ Event } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] The \code{Event} class is used to store the date and time of job events.

\item [Attributes] \

\classattribute{type}{\enum{}}{This enumeration can hold different values: \code{START}, \code{END}, \code{CREATION}, \code{SUBMISSION}, \code{OPTIMIZATION}, \code{CHECK}. It determines which event has occurred.}
\classattribute{time}{Datetime}{Date and time of the.}


\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}




\subsubsection[Job]{class \code{ Job } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents a job containing various \code{Operation} instances. A job can have different \code{Event} instances and is associated with one \code{AccessToken} that will be used while execution.

\item [Attributes] \

\classattribute{id}{\integer}{Unique ID of the job.}
\classattribute{token}{AccessToken}{Access token that will be used to authenticate the user while job execution.}
\classattribute{events}{Event[ ]}{Contains all events that already occurred.}
\classattribute{logentries}{LogEntry[ ]}{Contains all log entries that belong to the job.}
\classattribute{operations}{Operation[ ]}{Contains all operations that belong to the job.}
\classattribute{state}{\enum{}}{This enumeration can hold different values: \code{RUNNING}, \code{FINISHED}, \code{ERROR}, \code{WAITING}. They describe the current state of the job.}
\classattribute{priority}{\integer}{Priority of the job. This value will be used to schedule jobs.}
\classattribute{name}{String}{Name of the job.}
\classattribute{owner}{User}{The user who created the job.}


\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}




\subsubsection[Operation]{class \code{ Operation } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents a single operation of a \code{Job}, e.g. moving a file to a new directory.

\item [Attributes] \

\classattribute{job}{Job}{Reference to the job this operation is part of.}
\classattribute{logentries}{LogEntry[ ]}{Contains all log entries of the operation.}
\classattribute{type}{\enum{}}{This enumeration can hold one of those values: \code{COPY}, \code{MOVE}, \code{DELETE}, \code{RENAME}. It defines which action should be applied to the source and what the destination of the operation is. }
\classattribute{source}{String}{Path to the source file or source directory.}
\classattribute{target}{String}{Path to the target directory. In case of type \code{RENAME} target contains the new file/directory name.}
\classattribute{started\_on}{Datetime}{Date and time when the operation was started. Is \NULL if it was not started yet.}
\classattribute{finished\_on}{Datetime}{Date and time when the operation was finished. Is \NULL if it was not finished yet.}

\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}



\subsubsection[User]{class \code{ User } inherits from \code{ django.models.Model}}
\begin{description}

\item [Description] Represents a user.

\item [Attributes] \

\classattribute{id}{\integer}{Unix user ID of the represented user.}
\classattribute{tokens}{AccessToken[ ]}{Contains all \code{AccessToken} instances that belong to the user.}
\classattribute{group\_ids}{int[ ]}{All group IDs of the user.}
\classattribute{is\_admin}{\boolean}{\true if the user is an admin, \false otherwise.}
\classattribute{priority}{int}{Priority of the user. This value will probably be used by some scheduling algorithms.}

\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}




\subsubsection[LogEntry]{class \code{ LogEntry } inherits from \code{ django.models.Model}}
\begin{description}

\item [Description] Represents a log entry with a message and a date. \code{Job}, \code{Operation} and \code{Server} instances can have log entries.

\item [Attributes] \

\classattribute{user}{User}{Points to the user that created the \code{Job} or \code{Operation}. Can be \NULL in case of \code{Server} log messages or log entries that simply cannot be associated with an user.}
\classattribute{created\_on}{Datetime}{Date and time when the message was logged.}
\classattribute{message}{String}{Log message, e.g. error message.}
\classattribute{subject}{Object}{Contains the \code{Job}, \code{Operation} or \code{Server} instance the log entry is associated with.}
\classattribute{is\_error}{\boolean}{\true if the messages describes an error, \false if not.}


\item [Constructors] \

\none

\item [Methods] \
\classfunction{get\_subject(): Object}{Returns the associated object, e.g. an \code{Operation} instance.}
{
\codepos{\none}
}{
\codepos{Associated object.}
}

\end{description}




\subsubsection[Server]{class \code{ Server } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents a storage server (agent) and describes its capabilities.

\item [Attributes] \

\classattribute{logentries}{LogEntry[ ]}{Contains all log entries of this server.}
\classattribute{protocols}{Protocols[ ]}{Protocols the server supports.}
\classattribute{capacity}{\integer}{Storage server capacity in bytes.}
\classattribute{last\_activity}{Datetime}{Date and time when the agent running on the storage server polled the last time.}
\classattribute{token}{String}{Token the agent running on the storage server uses to authenticate at the job management server.}
\classattribute{hostname}{String}{Hostname of the storage server.}

\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}



\subsubsection[Protocol]{class \code{ Protocol } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents a file transfer protocol with a specific priority.

\item [Attributes] \

\classattribute{protocol}{String}{Name of the protocol.}
\classattribute{priority}{\integer}{Priority of this protocol. Can be used to define which protocol should be preferred when scheduling.}

\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}



\subsubsection[Connection]{class \code{ Connection } inherits from \code{django.models.Model}}
\begin{description}

\item [Description] Represents the connection speed between two servers via a specific protocol.

\item [Attributes] \

\classattribute{source}{Server}{Source storage server.}
\classattribute{target}{Server}{Target storage server.}
\classattribute{protocol}{String}{Name of the protocol that has the \code{speed} from source to target.}
\classattribute{speed}{\integer}{Speed of protocol from source to target.}

\item [Constructors] \

\none

\item [Methods] \

\none

\end{description}
