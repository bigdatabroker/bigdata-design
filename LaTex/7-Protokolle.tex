\section{Communication protocols}

The APIs use the JSON format to send and receive data.

\subsection{REST-API resources}
The REST-API is the client-side interface of the Job Management Server. All functions which a user can access are available through the REST-API. In this chapter all available resources including their endpoints are documented.

\subsubsection[Authentication]{Authentication pseudo resource}

\begin{description}

\item[Endpoint] \apiendpoint{/auth/}

\item[Methods] \apimethod{POST}

\item[Description] The authentication pseudo resource enables the clients to authenticate and legitimate themselves against the Job Management Server. After passing all required authentication data like username and password or private keys, the Job Management Server verifies the data and issues an access token if the verification was successful or returns an error message if the authentication was unsuccessful.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/auth/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Authenticates the user with the specified login data. Refer to chapter \ref{sec:api_auth} for information about the authentication workflow and authentication in general.}
			\item Parameters \
				\codepos{\apiparameterrequired{username}{Identifier of the user that wants to authenticate.}}
				\codepos{\apiparameterrequired{data}{Secret that is used to authenticate the user. Format of this parameter is not specified and depends on the used authentication backend.}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, authentication was successful}{Access token resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{403}{the authentication was not successful}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage


% --------------------------------------------------------------

\subsubsection[Job]{Job resource}

\begin{description}

\item[Endpoint] \apiendpoint{/job/}

\item[Methods] \apimethod{GET}, \apimethod{PUT}, \apimethod{POST}, \apimethod{DELETE}

\item[Description] The job resource enables clients to view, create, modify and delete jobs.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/job/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list of all jobs which the authenticated can access.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all job resource instances.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/}{PUT} \
		\begin{itemize}
			\item Description \
				\codepos{Creates a new job with the specified parameters.}
			\item Parameters \
				\codepos{\apiparameteroptional{name}{Name or short description of the job.}{date of the creation}}
				\codepos{\apiparameteroptional{priority}{Priority of the job.}{0 (neutral)}}
			\item Return Value \
				\codepos{\apireturnvalue{201}{ok, job was created successfully}{Job resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves information about the specified job.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Job resource instance.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Updates the specified job.}
			\item Parameters \
				\codepos{\apiparameteroptional{name}{Name or short description of the job.}{}}
				\codepos{\apiparameteroptional{priority}{Priority of the job.}{}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, job was successfully updated}{Updated job resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
				\codepos{\apireturnvalue{423}{job can't be updated, most likely because the job is already being executed}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/}{DELETE} \
		\begin{itemize}
			\item Description \
				\codepos{Deletes the specified job.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, job was successfully deleted}{\none}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
				\codepos{\apireturnvalue{423}{job can't be deleted, most likely because the job is already being executed}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/finalize/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Finalizes the specified job.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, job was successfully finalized}{Finalized job resource instance.}}
				\codepos{\apireturnvalue{400}{bad request, job was already finalized}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}


\newpage

% --------------------------------------------------------------

\subsubsection[Operation]{Operation resource}

\begin{description}

\item[Endpoint] \apiendpoint{/job/\{job\_id\}/operations/}

\item[Methods] \apimethod{GET}, \apimethod{PUT}, \apimethod{POST}, \apimethod{DELETE}

\item[Description] The operation resource enables clients to view, create, modify and delete operations.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/job/\{job\_id\}/operations/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list of all operations of the specified job.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all operations of the specified job.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/operations/}{PUT} \
		\begin{itemize}
			\item Description \
				\codepos{Creates a new operation for the specified job.}
			\item Parameters \
				\codepos{\apiparameterrequired{type}{Type of the operation.}}
				\codepos{\apiparameterrequired{source}{Source of the operation.}}
				\codepos{\apiparameterrequired{target}{Target of the operation.}}
			\item Return Value \
				\codepos{\apireturnvalue{201}{ok, operation was created successfully}{Operation resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
				\codepos{\apireturnvalue{423}{job can't be altered, most likely because the job is already being executed}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/operations/\{operation\_id\}/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves information about the specified operation.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Operation resource instance.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job or operation resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/operations/\{operation\_id\}/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Updates the specified operation.}
			\item Parameters \
				\codepos{\apiparameteroptional{type}{Type of the operation.}{}}
				\codepos{\apiparameteroptional{source}{Source of the operation.}{}}
				\codepos{\apiparameteroptional{target}{Target of the operation.}{}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, operation was successfully updated}{Updated operation resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job or operation resource doesn't exist}{Error message.}}
				\codepos{\apireturnvalue{423}{job can't be updated, most likely because the job is already being executed}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/operations/\{operation\_id\}/}{DELETE} \
		\begin{itemize}
			\item Description \
				\codepos{Deletes the specified operation.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, operation was successfully deleted}{\none}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
				\codepos{\apireturnvalue{423}{operation can't be deleted, most likely because the job is already being executed}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}


\newpage

% ------------------------ Server ----------------------------

\subsubsection[Server]{Server resource}

\begin{description}

\item[Endpoint] \apiendpoint{/server/}

\item[Methods] \apimethod{GET}, \apimethod{POST}, \apimethod{DELETE}

\item[Description] The server resource enables clients to view, modify and delete servers.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/server/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list of all registered servers.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all server resource instances.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access the servers}{Error message.}}
		\end{itemize}

	\item \apirequest{/server/\{server\_id\}/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves information about the specified server.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Server resource instance.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access the servers}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated server resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Updates the specified server.}
			\item Parameters \
				\codepos{\apiparameteroptional{capacity}{Capacity of the server.}{}}
				\codepos{\apiparameteroptional{hostname}{Hostname of the server.}{}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, server was successfully updated}{Updated server resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access the servers}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated server resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/}{DELETE} \
		\begin{itemize}
			\item Description \
				\codepos{Deletes the specified server.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, server was successfully deleted}{\none}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access the servers}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated server resource doesn't exist}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage

% ------------------------- Logentry ----------------------------

\subsubsection[Log entry]{Log entry resource}

\begin{description}

\item[Endpoint] \apiendpoint{/logentry/}

\item[Methods] \apimethod{GET}

\item[Description] The logentry resource enables clients to view a list of all logentries.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/logentry/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list of all logentries.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all log entry resource instances.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
		\end{itemize}
	\item \apirequest{/job/\{job\_id\}/logentry/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list of all log entries for the specified job.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all logentry resource instances for the specified job.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{associated job resource can't be accessed by the authenticated user}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; associated job resource doesn't exist}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage

% ----------------------------- Node ----------------------------
\subsubsection[Node]{Node resource}

\begin{description}

\item[Endpoint] \apiendpoint{/node/}

\item[Methods] \apimethod{GET}

\item[Description] The node resource enables clients to access the index. All nodex are identified by their unique URL; the API requires the URLs to be URLencoded in order for the API to work properly and reliable.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/node/\{URL\}/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves information about a specific node.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Node resource instance.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to view the specified path}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL specified}{Error message.}}
		\end{itemize}
	\item \apirequest{/node/\{URL\}/childs/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves a list containing all child nodes of the specified node.}
			\item Parameters \
				\codepos{\apiparameteroptional{level}{Number of levels below the specified node for which children should be returned.}{1}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all child node resource instance.}}
				\codepos{\apireturnvalue{400}{invalid or too high \code{level} parameter specified}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to view the specified path}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL specified}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage

% ----------------------------- Connection ----------------------------
\subsubsection[Connection]{Connection resource}

\begin{description}

\item[Endpoint] \apiendpoint{/server/\{server\_id\}/connection/}

\item[Methods] \apimethod{GET}, \apimethod{PUT}, \apimethod{POST}, \apimethod{DELETE}

\item[Description] The connection resource enables clients to access and manipulate the connection classifications between servers.

\item[Requests] \
\begin{itemize}
	\item \apirequest{/server/\{server\_id\}/connection/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves ALL available classifications between the specified server and other servers.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{List of all server connection classifications.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access server connections}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; server resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/connection/\{target\_server\_id\}}{PUT} \
		\begin{itemize}
			\item Description \
				\codepos{Adds a new connection classification between the two specified servers.}
			\item Parameters \
				\codepos{\apiparameterrequired{protocol}{Protocol for which the connection should be classified between the two servers.}}
				\codepos{\apiparameterrequired{speed}{Actual classification of the speed using the specified protocol between the two servers.}}
			\item Return Value \
				\codepos{\apireturnvalue{201}{ok, connection classification was crated successfully}{Connection resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access server connections}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; server resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/connection/\{target\_server\_id\}/\{protocol\}/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves information about the specified server connection.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Specified connection resource instance.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access server connections}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; connection resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/connection/\{target\_server\_id\}/\{protocol\}/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Updates the specified server connection.}
			\item Parameters \
				\codepos{\apiparameteroptional{speed}{Actual classification of the speed.}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok}{Updated connection resource instance.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access server connections}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; connection resource doesn't exist}{Error message.}}
		\end{itemize}
	\item \apirequest{/server/\{server\_id\}/connection/\{target\_server\_id\}/\{protocol\}/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Deletes the specified server connection.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, connection was successfully deleted}{\none}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{user is not allowed to access server connections}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; connection resource doesn't exist}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage

\subsection{Agent-API}
The Agent-API is a very simple, REST-like HTTP-API that is used for the communication of the agents with the Job Management Server. In the following chapter all supported requests are documented.

\begin{description}
\item[Requests] \
\begin{itemize}
	\item \apirequest{/server/}{PUT} \
		\begin{itemize}
			\item Description \
				\codepos{Allows agents to register themselves with the JMS. An agent access token is returned upon a successful authentication which can then be used by the agent to authenticate for future requests. Refer to chapter \ref{sec:api_auth} for information about the authentication workflow and authentication in general.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{201}{ok, registration successful}{server resource instance including the agent access token.}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
		\end{itemize}
	\item \apirequest{/task/}{GET} \
		\begin{itemize}
			\item Description \
				\codepos{Retrieves the next task for the authenticated agent from the JMS. A task includes all information like job, operation and access token.}
			\item Parameters \
				\none
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok: task available}{Task data.}}
				\codepos{\apireturnvalue{204}{no content: no task available}{\none}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{authentication failed}{Error message.}}
				\codepos{\apireturnvalue{404}{invalid URL; server resource doesn't exist}{Error message.}}
		\end{itemize}

	\item \apirequest{/status/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Updates the current status of the agent.}
			\item Parameters \
				\codepos{\apiparameterrequired{state}{Current state of the agent, e.g. WAITING, WORKING.}}
				\codepos{\apiparameteroptional{job\_id}{ID of the job the agent is currently executing.}{}}
				\codepos{\apiparameteroptional{operation\_id}{ID of the operation the agent is currently executing.}{}}
			\item Return Value \
				\codepos{\apireturnvalue{200}{ok, data accepted}{\none}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{authentication failed}{Error message.}}
		\end{itemize}
	\item \apirequest{/logentry/}{PUT} \
		\begin{itemize}
			\item Description \
				\codepos{Creates a log entry for the specified task (job, operation or storage server issues).}
			\item Parameters \
				\codepos{\apiparameterrequired{message}{Message that should be logged.}}
				\codepos{\apiparameterrequired{is\_error}{\true if the log message belongs to an error, \false otherwise.}}
				\codepos{\apiparameteroptional{job\_id}{ID of the job that the log entry should be created for.}{}}
				\codepos{\apiparameteroptional{operation\_id}{ID of the operation that the log entry should be created for.}{}}
			\item Return Value \
				\codepos{\apireturnvalue{201}{ok, log entry created}{\none}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{authentication failed or agent is not allowed to access the specified job}{Error message.}}
		\end{itemize}
	\item \apirequest{/tree/}{POST} \
		\begin{itemize}
			\item Description \
				\codepos{Passes file system structure data in the stat format which will then be processed by the JMS to update the index.}
			\item Parameters \
				\codepos{\apiparameteroptional{\none}{File system structure data in the stat format (HTTP request body).}{}}
			\item Return Value \
				\codepos{\apireturnvalue{202}{ok, data accepted}{\none}}
				\codepos{\apireturnvalue{400}{bad request}{Error message.}}
				\codepos{\apireturnvalue{401}{unauthenticated}{Error message.}}
				\codepos{\apireturnvalue{403}{authentication failed}{Error message.}}
		\end{itemize}
\end{itemize}

\end{description}

\newpage

\subsection{Authentication}
\label{sec:api_auth}
 All requests of the REST-API except \apirequest{/auth/}{POST} as well as all requests of the Agent-API except \apirequest{/server/}{PUT} are authenticated requests: in order to get a result, a valid access token must be passed along with the request using the appropriate header field. If no access token is provided, the API will return error code \code{401}. If the provided access token is invalid, error code \code{403} will be returned. \\ All API requests will only return data and allow operations that the authenticated user or agent has permissions for. \\ Access tokens are issued by authenticating using the \apirequest{/auth/}{POST} request (for clients) and \apirequest{/server/}{PUT} (for agents) and may exhibit an limited validity. \\
REST-API clients must pass the access token using the \code{X-BDB-Token} header field in the format
\begin{quote}
\code{X-BDB-Token: unix\_user\_id:token}
\end{quote}
Agents should utilize the header field \code{X-BDB-Agent-Token} in the format
\begin{quote}
\code{X-BDB-Agent-Token: token}
\end{quote}
\textbf{Attention}: All authenticated requests as well as all authentication requests must be performed using a secure, SSL encrypted connection. Performing a request that required a secure connection over an insecure connection will result in an error code \code{403} to be returned. In this case the server will not take further action.

\subsection{Sequence diagram}

\begin{figure}[H]
\noindent\makebox[\textwidth]{
\includegraphics[scale=0.90]{Bilder/Sequenzdiagramm_Kommunikation.pdf}
}
\caption{Sequence diagram illustrating the communication beween the Job Management Server and the agents / clients.}
\end{figure}
