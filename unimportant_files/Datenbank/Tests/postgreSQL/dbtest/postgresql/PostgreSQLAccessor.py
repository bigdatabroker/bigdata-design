import psycopg2
'''
Created on 22.05.2013

@author: Simon
'''

class PostgreSQLAccessor(object):

    '''
        Constructor: Initializes db connection and saves cursor
    '''
    def __init__(self):
        self.conn = psycopg2.connect("host='localhost' dbname='test' user='imperfun' password='bigdatabroker'")
        self.cursor = self.conn.cursor()
    
    '''
        returns the amount of elements in the tree
    '''
    def countElementsInTable(self):
        self.cursor.execute(""" SELECT COUNT(*) FROM t_hierarchy """)
        return self.cursor.fetchall();
        
    '''
        creates a root node and returns its ID
    '''
    def createNodeWithoutParent(self):   
        data = "FUUUUUUUUUUUUUUUUUUUUUUUUUU";
        stuffing = data*3
        sql = "INSERT INTO t_hierarchy (data, stuffing) VALUES ('" + data + "', '" + stuffing +"') RETURNING id;"
        self.cursor.execute(sql)    
        self.conn.commit()
        return self.cursor.fetchall()[0][0]   
    
    '''
        Creates a node with given parent id
    '''
    def createNode(self, parent):
        data = "FUUUUUUUUUUUUUUUUUUUUUUUUUU " + str(parent);
        stuffing = data*3
        sql = "INSERT INTO t_hierarchy (data, stuffing, parent) VALUES ('" + data + "', '" + stuffing +"', " + str(parent) + ") RETURNING id;"
        self.cursor.execute(sql)    
        self.conn.commit()
        return self.cursor.fetchall()[0][0]
    
    '''
        returns the path from a given element in the tree up to the root
    '''
    def getUpperPath(self, start, length = 1000000):
        self.cursor.execute("""
        WITH    RECURSIVE
                q AS
                (
                SELECT  h.*, 1 AS level
                FROM    t_hierarchy h
                WHERE   id = """ + str(start) + """
                UNION ALL
                SELECT  hp.*, level + 1
                FROM    q
                JOIN    t_hierarchy hp
                ON      hp.id = q.parent
                WHERE level < """ + str(length) + """
                )
        SELECT  id, parent, data, level
        FROM    q
        ORDER BY
                level ASC
        """)
        return self.cursor.fetchall();

    '''
        returns all children (subtree) of a given element up to a defined depth.
    '''
    def getChildren(self, start, depth = 1000000):
        self.cursor.execute("""
        WITH    RECURSIVE
                q AS
                (
                SELECT  h.*, 1 AS level
                FROM    t_hierarchy h
                WHERE   id = """ + str(start) + """
                UNION ALL
                SELECT  hp.*, level + 1
                FROM    q
                JOIN    t_hierarchy hp
                ON      hp.parent = q.id
                WHERE level < """ + str(depth) + """
                )
        SELECT  id, parent, data, stuffing, level
        FROM    q
        ORDER BY
                id ASC
        """)
        return self.cursor.fetchall();    
    
    '''
        clears table and resets auto increment counter
    '''
    def clear(self):
        self.cursor.execute("TRUNCATE TABLE t_hierarchy")
        self.cursor.execute("ALTER SEQUENCE t_hierarchy_id_seq RESTART WITH 1");
        self.conn.commit()
    
    '''
        moves node
    '''
    def changeParent(self, _id, parent):
        self.cursor.execute("UPDATE t_hierarchy SET parent = " + str(parent) + " WHERE id = " + str(_id))
        self.conn.commit()