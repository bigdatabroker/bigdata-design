'''
Created on 22.05.2013

@author: Simon
'''

class TreeManager(object):
    '''
        constructor takes db object to work with
    '''
    def __init__(self, db):
        self.db = db
    
    '''
        creates a tree
    '''    
    def createTree(self, depth, numOfChilds):
        for i in range(numOfChilds):
            parent = self.db.createNodeWithoutParent()
            self.doRecursion(depth - 1, numOfChilds, parent)    
            
        
    def doRecursion(self, depth, numOfChilds, parent):
        print (str(depth), end=" ")
        if (depth <= 0):
            return
        for i in range(numOfChilds):
            p = self.db.createNode(parent)
            self.doRecursion(depth - 1, numOfChilds, p)
            
    '''
        returns children of "start" up to depth levels
    '''
    def getTree(self, start, depth):
        return self.db.getChildren(start, depth)
    
    '''
        print the tree with intends
    '''
    def printTree(self, start, depth):
        tree = self.getTree(start, depth)
        
        for element in tree:
            print ("-" * int(element[4]) + " parent: "+ str(element[1]) + " / ID: " + str(element[0]) + " / LVL: " + str(element[4])) #+ " / data: " + str(element[2]))
    
    '''
        moves a subtree to another place
    '''
    def moveNode(self, _id, dest):
        self.db.changeParent(_id, dest)
        