import psycopg2
import pprint

#Define our connection string
conn_string = "host='localhost' dbname='test' user='postgres' password='PASSWORTHIEREINGEBEN!'"
 
# print the connection string we will use to connect
print (conn_string)
 
# get a connection, if a connect cannot be made an exception will be raised here
conn = psycopg2.connect(conn_string)
 
# conn.cursor will return a cursor object, you can use this cursor to perform queries
cursor = conn.cursor()
print ("Connected!\n")


def getUpperPath(start, length = 1000000):
    cursor.execute("""
    WITH    RECURSIVE
            q AS
            (
            SELECT  h.*, 1 AS level
            FROM    t_hierarchy h
            WHERE   id = """ + str(start) + """
            UNION ALL
            SELECT  hp.*, level + 1
            FROM    q
            JOIN    t_hierarchy hp
            ON      hp.id = q.parent
            WHERE level < """ + str(length) + """
            )
    SELECT  id, parent, data, level
    FROM    q
    ORDER BY
            level ASC
    """)
    return cursor.fetchall();

def getChildren(start, depth = 1000000):
    cursor.execute("""
    WITH    RECURSIVE
            q AS
            (
            SELECT  h.*, 1 AS level
            FROM    t_hierarchy h
            WHERE   id = """ + str(start) + """
            UNION ALL
            SELECT  hp.*, level + 1
            FROM    q
            JOIN    t_hierarchy hp
            ON      hp.parent = q.id
            WHERE level < """ + str(depth) + """
            )
    SELECT  id, parent, data, level
    FROM    q
    ORDER BY
            level ASC
    """)
    return cursor.fetchall();

records = getChildren(1, 6)
pprint.pprint(records)
